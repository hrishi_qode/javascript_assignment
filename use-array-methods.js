/* eslint-disable no-console */
function useArrayMethods() {
  // methods that return new array.
  const arr = [2, 3, 4, 5, 6, 6, 7, 8, { par: 20 }, [1, 2]];
  console.log(arr.concat([1, 2]));
  console.log(arr.filter((val) => typeof val === 'number'));
  console.log(arr.flat());
  console.log(arr.flatMap((val) => {
    if (typeof val === 'number') { return ++val; }
    return val;
  }));
  console.log(arr.map((val) => {
    if (typeof val === 'number') { return ++val; }
    return val;
  }));
  console.log(arr.slice(1, 3));
  console.log(arr.splice(1, 0, 'replaced'));
  // methods that mutate array.
  console.log(arr.copyWithin(2, 4, 6));
  console.log(arr.fill(30, 2, 5));
  console.log(arr.push(30));
  console.log(arr.pop());
  console.log(arr.reverse());
  arr.shift();
  arr.sort();
  arr.splice(1, 0, 'replaced');
  console.log(arr);
  arr.unshift(50);
  console.log(arr);
  // methods that iterate the array.
  console.log(arr.every((val) => val > 80));
  console.log(arr.find((val) => val > 40));
  console.log(arr.findIndex((val) => val > 20));
  arr.forEach((val) => {
    // iterate
  });
  console.log(arr.some((val) => val > 20));
}

export default useArrayMethods;
