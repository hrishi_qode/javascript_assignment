import useArrayMethods from './use-array-methods.js';
import useStringMethods from './use-string-methods.js';

useArrayMethods();
useStringMethods();
