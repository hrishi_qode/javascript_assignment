/* eslint-disable no-console */
function useStringMethods() {
  const str = 'string methods implementation';
  console.log(str.at(1));// accepts negative
  console.log(str.concat(' hello'));
  console.log(str.includes('hello'));
  console.log(str.endsWith('tion'));
  console.log(str.indexOf('method'));
  console.log(str.localeCompare('method'));
  console.log(`${str.padEnd(30)}|`);
  console.log(str.repeat(8));
  console.log(str.replace('s', 'repl'));
  console.log(str.search(/hod/));
  console.log(str.slice(2, 7));
  console.log(str.startsWith('str'));
  console.log(str.split(' '));
  console.log(str.substring(2, 7));// treats negative argument as 0.
  console.log(str.toLowerCase());
  console.log(str.toUpperCase());
}
export default useStringMethods;
